#!/usr/bin/env racket
#lang racket
(require racket/runtime-path (only-in xml write-xexpr) racket/rerequire
         "page.rkt"
         scribble/reader
         "lib.rkt")

(define-runtime-path resources "resources")
(define-runtime-path site "site")
(define-runtime-path source "source")
(define-runtime-path build.rkt "build.rkt")
(define-runtime-path page.rkt "page.rkt")
(define-runtime-path lib.rkt "lib.rkt")
(define-runtime-path defs.rkt "defs.rkt")
(define-runtime-path blog "blog/")

(module+ main
  (define run "")
  (define force-rebuild? #f)
  (command-line #:once-each
                [("-p" "--preview") "run frog server" (set! run "p")]
                [("-f" "--force-rebuild") "force full rebuild" (set! force-rebuild? #t)])
  #|
  (void (dynamic-rerequire "page.rkt" #:verbosity 'none))
  (define make-page (dynamic-require "page.rkt" 'make-page))
  (define watch #f)
  (set! watch #t)])
  (when watch (printf "watching for changes\n"))

  (let loop ()
  |#
  (define (i:read)
    (parameterize ([current-readtable (make-at-readtable #:command-readtable 'dynamic
                                                         #:datum-readtable 'dynamic)])
      (read (current-input-port))))

  (define source-files (map path->string (flatten (get-files source))))
  (define dest-files (map (compose path->string ->rel source->dest)
                          source-files))

  (define display-files
    (list "Home.html" "Publications.html"))
  (define build-ns (make-base-namespace))
  (define-namespace-anchor nsa)
  (define ns (namespace-anchor->namespace nsa))
  (parameterize ([current-namespace build-ns])
    (namespace-attach-module ns `(file ,(path->string lib.rkt)))
    (namespace-attach-module ns `(file ,(path->string defs.rkt)))
    (namespace-require 'racket)
    (namespace-require `(file ,(path->string lib.rkt)))
    (namespace-require `(file ,(path->string defs.rkt))))

  (define rebuild-all?
    (or force-rebuild?
        (for/or ([d dest-files])
          (or
           (not (file-exists? d))
           (< (file-or-directory-modify-seconds d)
              (file-or-directory-modify-seconds build.rkt))
           (< (file-or-directory-modify-seconds d)
              (file-or-directory-modify-seconds page.rkt))
           (< (file-or-directory-modify-seconds d)
              (file-or-directory-modify-seconds lib.rkt))
           (< (file-or-directory-modify-seconds d)
              (file-or-directory-modify-seconds defs.rkt))))))
  (for ([s (append source-files (list 'frog ))]
        [d (append dest-files (list "blog/_src/page-template.html"))]
        [i (in-naturals)]
        #:when
        (or (eq? s 'frog)
            rebuild-all?
            (< (file-or-directory-modify-seconds d)
               (file-or-directory-modify-seconds s))))
    (printf "building ~s\n" (path->string (->rel d)))
    (define e (explode-path d))
    (define dir (apply build-path (take e (sub1 (length e)))))
    (parameterize ([current-top
                    (apply build-path (current-top) (map (const 'up) (rest (explode-path dir))))])
      (define raw-body-expr
        (match s
          ['frog "@|contents|"]
          [else (with-input-from-file s i:read)]))

      (define x (eval (list 'quasiquote raw-body-expr) build-ns))

      (define body-xexpr
        (make-page
         (map (lambda (x) (list (string-append "/" x))) display-files)
         (hash-ref
          (hash
           "site/Publications.html" "Publications"
           "site/Home.html" "Home"
           "blog/_src/page-template.html" "Blog")
          d
          #f)
         x))

      (make-directory* (->rel dir))

      (with-output-to-file (->rel d)
        #:exists 'replace
        (thunk (write-xexpr body-xexpr)))))

  (define rsite (->rel (build-path site "resources")))
  (delete-directory/files rsite #:must-exist? #f)
  (copy-directory/files resources rsite)
  (unless (file-exists? (build-path site "index.html"))
    (make-file-or-directory-link (build-path site "Home.html")
                                 (build-path site "index.html")))
  (when (file-exists? "favicon.ico")
    (copy-file "favicon.ico" (build-path site "favicon.ico") #t))

  ;(when watch (sleep 1) (loop)))
  (parameterize ([current-directory blog])
    (system (~a "raco frog -b" run))))

(define (->rel path)
  (build-path
   (find-relative-path
    (simple-form-path (current-directory))
    (simple-form-path path))))

(define (cut-path p)
  (path->string (apply build-path (rest (explode-path p)))))

(define (get-files path)
  (cond [(directory-exists? path)
         (for/list ([p (in-directory path)]
                    #:when (regexp-match? #rx"rkt$" (path->string p)))
           p
           #;
           (build-path path p))]
        [else (error 'get-file "bad source dir: ~a" path)]))

(define (source->dest p)
  (define path (->rel p))
  (define base (rest (explode-path path)))
  (define src-file (path->string (last base)))
  (define rest-file
    (string-append (substring src-file 0 (- (string-length src-file) 3))
                   "html"))
  (path->string
   (apply build-path
          site
          (append (take base (sub1 (length base)))
                  (list rest-file)))))

(module+ test
  (require rackunit)
  (check-equal?
   (source->dest "source/a.rkt")
   "build/a.html"))
