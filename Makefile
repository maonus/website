main:
	racket build.rkt

deploy: main
	scp -r ./site/* zappa:public_html
	ssh zappa 'chmod -R 755 public_html'
