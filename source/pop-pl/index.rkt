,(cite-page
  #:title "POP-PL: A Patient-Oriented Prescription Programming Language"
  #:authors (list "Florence"
                  "Fetscher"
                  "Flatt"
                  "Temps"
                  "St-Amour"
                  "Kiguradze"
                  "West"
                  "Niznik"
                  "Yarnold"
                  "Findler"
                  "Belknap")
  #:urls `(["Journal Version (TOPLAS 2018)" . , poppl2-pdf]
           ["Original (GPCE 2015)" . ,poppl-pdf]
           ["Supplementary Materials" . ,poppl-supp]
           ["Implementation" . ,poppl-url])
  #:abstract
  (div
    @p{Medical professionals have long used algorithmic thinking to describe and implement
       health care processes without the benefit of the conceptual framework provided by a programming
       language. Instead, medical algorithms are expressed using English, flowcharts, or data
       tables. This results in prescriptions that are difficult to understand, hard to debug, and
       awkward to reuse.}
    @p{This paper reports on the design and evaluation of a domain-specific programming language, POP-PL,
       for expressing medical algorithms. The design draws on the experience of researchers in two
       disciplines, programming languages and medicine. The language is based around the idea that programs
       and humans have complementary strengths, that when combined can make for safer, more accurate
       performance of prescriptions.}
    @p{We implemented a prototype of our language and evaluated its design by writing prescriptions in the
       new language and administering a usability survey to medical professionals. This formative
       evaluation suggests that medical prescriptions can be conveyed by a programming language's mode of
       expression and provides useful information for refining the language.  Analysis of the survey
       results suggests that medical professionals can understand and correctly modify programs in
       POP-PL.}))
